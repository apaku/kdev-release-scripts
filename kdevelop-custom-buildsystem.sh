#!/bin/sh

# Simple script to remind me of the correct parameters for calling the ruby script
# Simply takes the version number and expects a corresponding git tag v<version> to exist
/usr/bin/ruby1.8 ./kdevelop-custom-buildsystem.rb --git-branch v$1 -b stable -m 75 -u apaku -p ssh -v $1 --notification
